<?php

namespace IDerysh\VendorsForElogic\Model;

use Magento\Framework\File\Name;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Helper\File\Storage\Database;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class ImageUploader extends \Magento\Catalog\Model\ImageUploader
{
    const BASE_TMP_PATH = 'vendor/tmp/logos';
    const BASE_PATH = 'vendor/logos';

    public function __construct(
        Database $coreFileStorageDatabase,
        Filesystem $filesystem,
        UploaderFactory $uploaderFactory,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger,
        $baseTmpPath = 'vendor/tmp/logos',
        $basePath = 'vendor/logos',
        $allowedExtensions = ['jpg' => 'jpg',
                              'jpeg' => 'jpeg',
                              'png' => 'png',
                              'gif' => 'gif',
            ],
        $allowedMimeTypes = [],
        Name $fileNameLookup = null
    ) {
        parent::__construct(
            $coreFileStorageDatabase,
            $filesystem,
            $uploaderFactory,
            $storeManager,
            $logger,
            $baseTmpPath,
            $basePath,
            $allowedExtensions,
            $allowedMimeTypes,
            $fileNameLookup
        );
    }
}
