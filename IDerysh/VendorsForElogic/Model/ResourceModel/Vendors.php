<?php

namespace IDerysh\VendorsForElogic\Model\ResourceModel;

class Vendors extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('vendors_for_elogic', 'id');
    }
}

