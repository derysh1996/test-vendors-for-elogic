<?php

namespace IDerysh\VendorsForElogic\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface VendorsRepositoryInterface
{

    /**
     * Save Vendors
     * @param \IDerysh\VendorsForElogic\Api\Data\VendorsInterface $vendors
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \IDerysh\VendorsForElogic\Api\Data\VendorsInterface $vendors
    );

    /**
     * Retrieve Vendors
     * @param string $vendorsId
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($vendorsId);

    /**
     * Retrieve Vendors matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsInterface[]
     */
    public function getAllItems();

    /**
     * Delete Vendors
     * @param \IDerysh\VendorsForElogic\Api\Data\VendorsInterface $vendors
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \IDerysh\VendorsForElogic\Api\Data\VendorsInterface $vendors
    );

    /**
     * Delete Vendors by ID
     * @param string $vendorsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($vendorsId);
}

