<?php

namespace IDerysh\VendorsForElogic\Controller\Adminhtml\Vendors;

use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{
    protected $dataPersistor;
    protected $vendorImageUploader;

    /**
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        ImageUploader $vendorImageUploader
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->vendorImageUploader = $vendorImageUploader;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');

            $model = $this->_objectManager->create(\IDerysh\VendorsForElogic\Model\Vendors::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Vendor is no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $data = $this->_filterVendorData($data);
            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Vendor.'));
                $this->dataPersistor->clear('vendors_for_elogic');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Vendor.'));
            }

            $this->dataPersistor->set('vendors_for_elogic', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Set image to null if it is deleted
     *
     * @param array $rawData
     * @return array
     */
    public function _filterVendorData($data)
    {
        if (isset($data['logo'][0]['name']) && isset($data['logo'][0]['tmp_name'])) {
            $data['logo'] = $data['logo'][0]['name'];
            $this->vendorImageUploader->moveFileFromTmp($data['logo']);
        } elseif (isset($data['logo'][0]['image']) && !isset($data['logo'][0]['tmp_name'])) {
            $data['logo'] = $data['logo'][0]['image'];
        } else {
            $data['logo'] = null;
        }

        return $data;
    }
}
