<?php

namespace IDerysh\VendorsForElogic\Controller\Adminhtml\Vendors;

use IDerysh\VendorsForElogic\Model\Vendors;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \IDerysh\VendorsForElogic\Controller\Adminhtml\Vendors
{
    protected $resultPageFactory;
    /**
     * @var Vendors
     */
    private $vendors;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        Vendors $vendors
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->vendors = $vendors;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        if ($id) {
            $this->vendors->load($id);
            if (!$this->vendors->getId()) {
                $this->messageManager->addErrorMessage(__('This Vendor is no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('vendors_for_elogic', $this->vendors);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Vendor') : __('New Vendor'),
            $id ? __('Edit Vendor') : __('New Vendor')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Vendor'));
        $resultPage->getConfig()->getTitle()->prepend($this->vendors->getId() ? __('Edit Vendor %1', $this->vendors->getId()) : __('New Vendor'));
        return $resultPage;
    }
}
