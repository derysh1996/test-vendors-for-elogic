<?php

namespace IDerysh\VendorsForElogic\Model\ResourceModel\Vendors;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \IDerysh\VendorsForElogic\Model\Vendors::class,
            \IDerysh\VendorsForElogic\Model\ResourceModel\Vendors::class
        );
    }
}

