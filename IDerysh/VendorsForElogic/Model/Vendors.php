<?php

namespace IDerysh\VendorsForElogic\Model;

use IDerysh\VendorsForElogic\Api\Data\VendorsInterface;
use IDerysh\VendorsForElogic\Api\Data\VendorsInterfaceFactory;
use IDerysh\VendorsForElogic\Model\ResourceModel\Vendors\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Vendors extends \Magento\Framework\Model\AbstractModel
{
    protected $vendorsDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'vendors_for_elogic';

    /**
     * @param Context $context
     * @param Registry $registry
     * @param VendorsInterfaceFactory $vendorsDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param ResourceModel\Vendors $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        VendorsInterfaceFactory $vendorsDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Vendors $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->vendorsDataFactory = $vendorsDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve vendors model with vendors data
     * @return VendorsInterface
     */
    public function getDataModel()
    {
        $vendorsData = $this->getData();

        $vendorsDataObject = $this->vendorsDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $vendorsDataObject,
            $vendorsData,
            VendorsInterface::class
        );

        return $vendorsDataObject;
    }
}
