<?php

namespace IDerysh\VendorsForElogic\Model;

use IDerysh\VendorsForElogic\Api\Data\VendorsInterface;
use IDerysh\VendorsForElogic\Api\Data\VendorsInterfaceFactory;
use IDerysh\VendorsForElogic\Api\Data\VendorsSearchResultsInterfaceFactory;
use IDerysh\VendorsForElogic\Api\VendorsRepositoryInterface;
use IDerysh\VendorsForElogic\Model\ResourceModel\Vendors as ResourceVendors;
use IDerysh\VendorsForElogic\Model\ResourceModel\Vendors\CollectionFactory as VendorsCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface as SearchCriteriaInterfaceAlias;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class VendorsRepository implements VendorsRepositoryInterface
{
    protected $resource;

    protected $vendorsFactory;

    protected $vendorsCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataVendorsFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceVendors $resource
     * @param VendorsFactory $vendorsFactory
     * @param VendorsInterfaceFactory $dataVendorsFactory
     * @param VendorsCollectionFactory $vendorsCollectionFactory
     * @param VendorsSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceVendors $resource,
        VendorsFactory $vendorsFactory,
        VendorsInterfaceFactory $dataVendorsFactory,
        VendorsCollectionFactory $vendorsCollectionFactory,
        VendorsSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->vendorsFactory = $vendorsFactory;
        $this->vendorsCollectionFactory = $vendorsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataVendorsFactory = $dataVendorsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        VendorsInterface $vendors
    ) {
        $vendorsData = $this->extensibleDataObjectConverter->toNestedArray(
            $vendors,
            [],
            VendorsInterface::class
        );

        $vendorsModel = $this->vendorsFactory->create()->setData($vendorsData);

        try {
            $this->resource->save($vendorsModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the vendor: %1',
                $exception->getMessage()
            ));
        }
        return $vendorsModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($vendorsId)
    {
        $vendors = $this->vendorsFactory->create();
        $this->resource->load($vendors, $vendorsId);
        if (!$vendors->getId()) {
            throw new NoSuchEntityException(__('Vendor with id "%1" does not exist.', $vendorsId));
        }
        return $vendors->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterfaceAlias $criteria
    ) {
        $collection = $this->vendorsCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            VendorsInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    public function getAllItems()
    {
        $vendors = $this->vendorsCollectionFactory->create();
        return $vendors->getItems();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        VendorsInterface $vendors
    ) {
        try {
            $vendorsModel = $this->vendorsFactory->create();
            $this->resource->load($vendorsModel, $vendors->getVendorId());
            $this->resource->delete($vendorsModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Vendor: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($vendorsId)
    {
        return $this->delete($this->get($vendorsId));
    }
}
