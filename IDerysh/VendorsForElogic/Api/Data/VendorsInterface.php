<?php

namespace IDerysh\VendorsForElogic\Api\Data;

interface VendorsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const VENDOR = 'vendor';
    const ID = 'id';
    const DESCRIPTION = 'description';
    const LOGO = 'logo';

    /**
     * Get id
     * @return string|null
     */
    public function getVendorId();

    /**
     * Set id
     * @param string $vendorsId
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsInterface
     */
    public function setVendorId($vendorsId);

    /**
     * Get vendor
     * @return string|null
     */
    public function getVendor();


    /**
     * Set vendor
     * @param string $vendor
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsInterface
     */
    public function setVendor($vendor);

    /**
     * @param string $description
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsInterface
     */
    public function setDescription(string $description = '');

    /**
     * @return mixed
     */
    public function getDescription();

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \IDerysh\VendorsForElogic\Api\Data\VendorsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \IDerysh\VendorsForElogic\Api\Data\VendorsExtensionInterface $extensionAttributes
    );
}
