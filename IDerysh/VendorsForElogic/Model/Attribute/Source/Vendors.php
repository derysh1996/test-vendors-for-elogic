<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace IDerysh\VendorsForElogic\Model\Attribute\Source;

use IDerysh\VendorsForElogic\Model\VendorsRepository;

class Vendors extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    private $vendorsRepository;

    public function __construct(VendorsRepository $vendorsRepository)
    {
        $this->vendorsRepository = $vendorsRepository;
    }

    /**
     * Get all options
     * @return array
     */
    public function getAllOptions()
    {
        foreach ($this->vendorsRepository->getAllItems() as $item) {
            $this->_options[] =
                [
                'label' => __($item->getVendor()),
                'value' => __($item->getId())
                ];
        }
        return $this->_options;
    }
}
