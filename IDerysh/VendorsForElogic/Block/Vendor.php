<?php

namespace IDerysh\VendorsForElogic\Block;

use Magento\Backend\Block\Template\Context;
use Magento\Eav\Model\Config;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\StoreManagerInterface;
use IDerysh\VendorsForElogic\Model\VendorsFactory;
use IDerysh\VendorsForElogic\Setup\Patch\Data\AddVendorAttribute;

class Vendor extends Template
{
    protected $_registry;
    protected $eavConfig;
    protected $productRepository;
    protected $storeManager;
    protected $vendorFactory;

    public function __construct(
        Context $context,
        Registry $registry,
        Config $eavConfig,
        StoreManagerInterface $storeManager,
        VendorsFactory $vendorFactory,
        array $data = []
    ) {
        $this->eavConfig = $eavConfig;
        $this->_registry = $registry;
        $this->storeManager = $storeManager;
        $this->vendorFactory = $vendorFactory;
        parent::__construct($context, $data);
    }

    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    public function getImageUrlsArray()
    {
        $result = [];
        $product = $this->getCurrentProduct();
        $ids = $product->getData(AddVendorAttribute::ATTRIBUTE_CODE);

        if (!empty(trim($ids))) {
            $allVendors = $this->getAllVendorsArray();

            $urlWithoutBaseName = $this->storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ) . 'vendor/logos/';

            foreach (explode(',', $ids) as $vendorId) {
                !empty($fileName = $allVendors[$vendorId]["logo"]) and $result[] = $urlWithoutBaseName . $fileName;
            }
        }
        if (!empty($result)) {
            return $result;
        }

        return [];
    }

    protected function getAllVendorsArray()
    {
        $vendorSetup = $this->vendorFactory->create();
        if (!empty($vendors = $vendorSetup->getCollection()->getData())) {
            return array_column($vendors, null, 'id');
        }
        return [];
    }
}
