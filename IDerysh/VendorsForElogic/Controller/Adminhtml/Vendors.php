<?php

namespace IDerysh\VendorsForElogic\Controller\Adminhtml;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;

abstract class Vendors extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'IDerysh_VendorsForElogic::top_level';
    protected $_coreRegistry;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Vendors for Elogic'), __('Vendors for Elogic'))
            ->addBreadcrumb(__('All vendors'), __('All vendors'));
        return $resultPage;
    }
}

