<?php

namespace IDerysh\VendorsForElogic\Model\Data;

use IDerysh\VendorsForElogic\Api\Data\VendorsInterface;

class Vendors extends \Magento\Framework\Api\AbstractExtensibleObject implements VendorsInterface
{

    /**
     * Get id
     * @return string|null
     */
    public function getVendorId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set vendor_id
     * @param string $vendorsId
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsInterface
     */
    public function setVendorId($vendorsId)
    {
        return $this->setData(self::ID, $vendorsId);
    }

    /**
     * Get vendor
     * @return string|null
     */
    public function getVendor()
    {
        return $this->getData(self::VENDOR);
    }

    /**
     * Set vendor
     * @param string $vendor
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsInterface
     */
    public function setVendor($vendor)
    {
        return $this->setData(self::VENDOR, $vendor);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \IDerysh\VendorsForElogic\Api\Data\VendorsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \IDerysh\VendorsForElogic\Api\Data\VendorsExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * @param string $description
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsInterface
     */
    public function setDescription(string $description = ''): \IDerysh\VendorsForElogic\Api\Data\VendorsInterface
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }
}

