<?php

namespace IDerysh\VendorsForElogic\Controller\Adminhtml\Vendors\Logo;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\Controller\ResultFactory;

class Upload extends Action
{
    /**
     * Image uploader
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * @param Context $context
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        Context $context,
        ImageUploader $imageUploader
    ) {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
    }

    /**
     * Check admin permissions for this controller
     * @return boolean
     */
    public function _isAllowed()
    {
//        return $this->_authorization->isAllowed('IDerysh_VendorsForElogic::vendors_save');
        return true;
    }

    /**
     * Upload file controller action
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->imageUploader->saveFileToTmpDir('logo');

        $result['cookie'] = [
            'name' => $this->_getSession()->getName(),
            'value' => $this->_getSession()->getSessionId(),
            'lifetime' => $this->_getSession()->getCookieLifetime(),
            'path' => $this->_getSession()->getCookiePath(),
            'domain' => $this->_getSession()->getCookieDomain(),
        ];
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
