<?php

namespace IDerysh\VendorsForElogic\Api\Data;

interface VendorsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Vendors list.
     * @return \IDerysh\VendorsForElogic\Api\Data\VendorsInterface[]
     */
    public function getItems();

    /**
     * Set vendor list.
     * @param \IDerysh\VendorsForElogic\Api\Data\VendorsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

